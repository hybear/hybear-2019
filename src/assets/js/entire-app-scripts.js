/* eslint-disable */
// import fullpageJS from 'fullpage.js';
import * as THREE from 'three';
// import * as $ from 'jquery';
import anime from 'animejs';
import particlesJS from 'particles.js';

// BACKGROUND CANVAS RENDER
function createHeroInteractive($container, BG1, BG2) {
  'use strict';
  // prettier-ignore
  var renderer, scene, camera, pixelRatio = Math.min(window.devicePixelRatio, 2),
  backgroundScene, backgroundCamera,
  geometryBackgroundScene, materialBackgroundScene, meshBackgroundScene,
  fov = 45;

  var vertexShaderBackgroundScene = `varying vec2 vUv;
  void main() {
      vUv = uv;
      vec3 pos = position;
      gl_Position = projectionMatrix * modelViewMatrix * vec4(pos, 1.0);
  }`;
  var fragmentShaderBackgroundScene = `
      vec2 cover(vec2 screenDimensions, vec2 textureDimensions) {
          vec2 s = screenDimensions;
          vec2 i = textureDimensions;
          vec2 st = gl_FragCoord.xy / s;
          float rs = s.x / s.y;
          float ri = i.x / i.y;
          vec2 new = rs < ri ? vec2(i.x * s.y / i.y, s.y) : vec2(s.x, i.y * s.x / i.x);
          vec2 offset = (rs < ri ? vec2((new.x - s.x) / 2.0, 0.0) : vec2(0.0, (new.y - s.y) / 2.0)) / new;
          vec2 uv = st * s / new + offset;
          return uv;
      }
  
      vec2 uvCover(in vec2 _uv, vec2 screenDimensions, vec2 textureDimensions) {
          vec2 s = screenDimensions;
          vec2 i = textureDimensions;
          float rs = s.x / s.y;
          float ri = i.x / i.y;
          vec2 new = rs < ri ? vec2(i.x * s.y / i.y, s.y) : vec2(s.x, i.y * s.x / i.x);
          vec2 offset = (rs < ri ? vec2((new.x - s.x) / 2.0, 0.0) : vec2(0.0, (new.y - s.y) / 2.0)) / new;
          vec2 uv = _uv * s / new + offset;
          return uv;
      }
      vec3 mod289(vec3 x) { return x - floor(x * (1.0 / 289.0)) * 289.0; }
      vec2 mod289(vec2 x) { return x - floor(x * (1.0 / 289.0)) * 289.0; }
      vec3 permute(vec3 x) { return mod289(((x*34.0)+1.0)*x); }
  
      float snoise(vec2 v) {
          const vec4 C = vec4(0.211324865405187,  // (3.0-sqrt(3.0))/6.0
                              0.366025403784439,  // 0.5*(sqrt(3.0)-1.0)
                              -0.577350269189626,  // -1.0 + 2.0 * C.x
                              0.024390243902439); // 1.0 / 41.0
          vec2 i  = floor(v + dot(v, C.yy) );
          vec2 x0 = v -   i + dot(i, C.xx);
          vec2 i1;
          i1 = (x0.x > x0.y) ? vec2(1.0, 0.0) : vec2(0.0, 1.0);
          vec4 x12 = x0.xyxy + C.xxzz;
          x12.xy -= i1;
          i = mod289(i); // Avoid truncation effects in permutation
          vec3 p = permute( permute( i.y + vec3(0.0, i1.y, 1.0 ))
              + i.x + vec3(0.0, i1.x, 1.0 ));
  
          vec3 m = max(0.5 - vec3(dot(x0,x0), dot(x12.xy,x12.xy), dot(x12.zw,x12.zw)), 0.0);
          m = m*m ;
          m = m*m ;
          vec3 x = 2.0 * fract(p * C.www) - 1.0;
          vec3 h = abs(x) - 0.5;
          vec3 ox = floor(x + 0.5);
          vec3 a0 = x - ox;
          m *= 1.79284291400159 - 0.85373472095314 * ( a0*a0 + h*h );
          vec3 g;
          g.x  = a0.x  * x0.x  + h.x  * x0.y;
          g.yz = a0.yz * x12.xz + h.yz * x12.yw;
          return 130.0 * dot(m, g);
      }
      uniform sampler2D u_textureBg;
      uniform sampler2D u_texture;
      uniform vec2 u_dimensions;
      uniform vec2 u_textureDimensions;
      uniform float u_time;
      uniform vec2 u_mousePosition;
  
      void main() {
          vec2 st = gl_FragCoord.xy / u_dimensions;
          vec2 texSt = st;
          st.x *= u_dimensions.x / u_dimensions.y;
  
          vec3 bg = texture2D(u_textureBg, uvCover(texSt, u_dimensions, u_textureDimensions)).rgb;
          vec3 tex = texture2D(u_texture, uvCover(texSt, u_dimensions, u_textureDimensions)).rgb;
          vec3 color = tex;
          float noise = snoise(texSt * 0.7 + u_time * 0.05) + snoise(texSt * 0.4 - u_time * 0.03 + 0.3) + snoise(texSt * 0.6 - u_time * 0.01 + 0.5);
          noise = smoothstep(0.399, 0.4, noise);
          color = mix(
              color,
              bg,
              noise
          );
          gl_FragColor = vec4(color, 1.);
      }`;
  var mousePosition = {
      x: 0.5,
      y: 0.5,
  };
  var v2MousePosition = new THREE.Vector2(mousePosition.x, mousePosition.y);

  var start = Date.now();
  var fixedTime = 0,
      timeOffset = 0,
      dynamicTime = 0;

  var w, h, isMobile;

  var camX = 0;
  var camY = 0;

  var isFocused = true,
      isInited = false,
      inViewport = false;

  window.addEventListener('blur', function() {
      isFocused = false;
  });
  window.addEventListener('focus', function() {
      isFocused = true;
      if (isInited) {
          render();
      }
  });

  function updateSize() {
      w = $container.offsetWidth;
      h = $container.offsetHeight;
      isMobile = w < 800;
      pixelRatio = isMobile ? Math.min(window.devicePixelRatio, 2) : 1;
  }

  var loadedAssetsCount = 0;
  var requiredAssetsCount = 2;

  function hasEverythingLoaded() {
      if (loadedAssetsCount === requiredAssetsCount) {
          init();
      }else{
        console.log(loadedAssetsCount)
      }
  }
  // prettier-ignore
  function M(b,c,f,g,h){var e=-Math.abs(h);this.value=b;this.s=!0;var a=0;this.update=function(d){a+=(d-this.value)*f;a*=g;this.value+=a;this.value<b?(this.value=b,0>a&&(a*=e)):this.value>c&&(this.value=c,0<a&&(a*=e));this.s=Math.abs(a)<.001*(c-b)&&.1>Math.abs(this.value-d)?!0:!1;return this.value};this.setValue=function(d){a=0;this.s=!0;return this.value=Math.min(c,Math.max(b,d))}}

  var textureLoader = new THREE.TextureLoader();

  var imageTexture1Width, imageTexture1Height;
  var imageTexture1 = textureLoader.load(BG1, function(texture) {
      imageTexture1Width = texture.image.width;
      imageTexture1Height = texture.image.height;
      texture.generateMipmaps = false;
      texture.wrapS = texture.wrapT = THREE.ClampToEdgeWrapping;
      texture.minFilter = THREE.LinearFilter;

      loadedAssetsCount++;
      hasEverythingLoaded();
  });

  var imageTexture2Width, imageTexture2Height;
  var imageTexture2 = textureLoader.load(BG2, function(texture) {
      imageTexture2Width = texture.image.width;
      imageTexture2Height = texture.image.height;
      texture.generateMipmaps = false;
      texture.wrapS = texture.wrapT = THREE.ClampToEdgeWrapping;
      texture.minFilter = THREE.LinearFilter;

      loadedAssetsCount++;
      hasEverythingLoaded();
  });

  function init() {
      updateSize();
      scene = new THREE.Scene();

      camera = new THREE.PerspectiveCamera(fov, w / h, 1, 500);
      camera.position.z = isMobile ? 200 : 150;

      scene.add(camera);

      materialBackgroundScene = new THREE.ShaderMaterial({
          uniforms: {
              u_textureBg: {type: 't', value: imageTexture1},
              u_texture: {type: 't', value: imageTexture2},
              u_textureDimensions: {
                  type: 'v2',
                  value: new THREE.Vector2(
                      imageTexture2Width,
                      imageTexture2Height
                  ),
              },
              u_dimensions: {
                  type: 'v2',
                  value: new THREE.Vector2(w * pixelRatio, h * pixelRatio),
              },
              u_time: {type: 'f', value: 0},
              u_mousePosition: {type: 'v2', value: v2MousePosition},
          },
          fragmentShader: fragmentShaderBackgroundScene,
          vertexShader: vertexShaderBackgroundScene,
      });
      var backgroundMesh = new THREE.Mesh(
          new THREE.PlaneGeometry(2, 2, 0),
          materialBackgroundScene
      );
      backgroundScene = new THREE.Scene();
      backgroundCamera = new THREE.Camera();
      backgroundScene.add(backgroundCamera);
      backgroundScene.add(backgroundMesh);

      renderer = new THREE.WebGLRenderer({antialias: true, alpha: false});
      renderer.setPixelRatio(pixelRatio);
      renderer.setSize(w, h);
      renderer.setClearColor(new THREE.Color('rgb(255,255,255)'));

      renderer.autoClear = false;

      $container.appendChild(renderer.domElement);

      window.addEventListener('resize', handleResize);
      if ('ontouchstart' in window) {
          document.body.addEventListener('touchmove', handleTouchMove);
      } else {
          document.body.addEventListener('mousemove', handleMouseMove);
      }

      isInited = true;
      render();
  }

  function handleResize() {
      updateSize();
      renderer.setSize(w, h);
      renderer.setPixelRatio(pixelRatio);
      camera.aspect = w / h;
      camera.position.z = isMobile ? 200 : 150;

      var v2WH = new THREE.Vector2(w, h);
      materialBackgroundScene.uniforms.u_dimensions.value = v2WH;

      camera.updateProjectionMatrix();
  }

  function handleTouchMove(e) {
      var touch = e.touches[0];
      mousePosition.x = touch.pageX / w;
      mousePosition.y = touch.pageY / h;
  }

  function handleMouseMove(e) {
      mousePosition.x = e.pageX / w;
      mousePosition.y = e.pageY / h;
  }

  function render(timestamp) {
      if (isFocused) {
          camX =
              ((mousePosition.x - 0.5) * (isMobile ? 100 : 60) -
                  camera.position.x) *
              0.05;
          camY =
              ((mousePosition.y - 0.5) * (isMobile ? 60 : 40) -
                  camera.position.y) *
              0.05;

          fixedTime = 0.0001 * (Date.now() - start);

          timeOffset += (Math.abs(camX) + Math.abs(camY)) / 20;
          dynamicTime = fixedTime + timeOffset;
          materialBackgroundScene.uniforms.u_time.value = dynamicTime;

          camera.position.x += camX;
          camera.position.y += camY;
          camera.position.z += camY;
          camera.lookAt(scene.position);

          renderer.clear();
          renderer.render(backgroundScene, backgroundCamera);

          requestAnimationFrame(render);
      }
  }
}
// MORPHING WAVES
function MorphingBG(el) {
  var Svgpaths = Array.from(el.querySelectorAll('path'));
  function animate() {
    Svgpaths.forEach((path) => {
      setTimeout(() => {
        anime({
          targets: path,
          duration: anime.random(2000,8000),
          easing: 'cubicBezier(0.5,0,0.5,1)',  
          d: path.getAttribute('pathdata:id'),
          loop: true,
          direction: 'alternate'
        });
      }, anime.random(100,0));
    });
  }
  animate();
}
function InstallApp(){
    var deferredPrompt;

    window.addEventListener('beforeinstallprompt', function (e) {
    // Prevent Chrome 67 and earlier from automatically showing the prompt
    e.preventDefault();
    // Stash the event so it can be triggered later.
    deferredPrompt = e;

    showAddToHomeScreen();

    });
    function showAddToHomeScreen() {

        var a2hsBtn = document.querySelector(".ad2hs-prompt");
        
        a2hsBtn.style.display = "block";
        
        a2hsBtn.addEventListener("click", addToHomeScreen);
    
    }
    function addToHomeScreen() {  
        var a2hsBtn = document.querySelector(".ad2hs-prompt");  // hide our user interface that shows our A2HS button
        a2hsBtn.style.display = 'none';  // Show the prompt
        deferredPrompt.prompt();  // Wait for the user to respond to the prompt
        deferredPrompt.userChoice
            .then(function(choiceResult){

                if (choiceResult.outcome === 'accepted') {
                    console.log('User accepted the A2HS prompt');
                } else {
                    console.log('User dismissed the A2HS prompt');
                }

                deferredPrompt = null;
            });
    }
}
// LOADING APP
function Loading(){
    var val = 0;
    var $circle = document.querySelector('#bar');
    var r = $circle.getAttribute('r');
    var c = Math.PI*(r*2);
    var pct = ((100-0)/100)*c;
  
    $circle.style.strokeDashoffset = pct;
    function getRndInteger(min, max) {
      val = Math.floor(Math.random() * (max - min)) + min;
      console.log(val);
    }
    var checkload = setInterval(() => {
    if(val < 100) {
        getRndInteger(val,101);
        if (val < 0) { val = 0;}
        if (val > 100) { val = 100;}
        
        var pct = ((100-val)/100)*c;
        
        $circle.style.strokeDashoffset = pct;
      }else if(val == 100){
        // document.querySelector(".circle-loader").style.transform = "scale(0)";
        // document.querySelector(".circle-loader").style.opacity = "0";
        document.querySelector(".bear-paw path").style.fill = "#E27945";
        setTimeout(() => {
          document.querySelector(".loading").style.opacity = "0";
          setTimeout(() => {
            document.querySelector(".loading").style.display = "none";
          }, 500);
        }, 1000);
      }
    }, 1000);
}

// function randomFeathers(){
//     var feathers = Math.floor((Math.random() * 20) + 1),
//     featherpositionY = Math.floor((Math.random() * 100) + 1),
//     featherpositionX = Math.floor((Math.random() * 100) + 1),
//     featherRotation = Math.floor((Math.random() * 360) + 1),
//     featherColors = Math.floor((Math.random() * 2) + 1),
//     html = "";
//     for(var i = 0; i < feathers; i++){
//         html+='<svg class="absolute" style="left:' + featherpositionY +'%; top:'+ featherpositionX +'%; transform: rotate('+ featherRotation + 'deg);" width="24" height="69" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0 20.3813c.607595.6037 1.64919 1.5956 2.77758 2.5444 1.12839.9487 2.30018 1.8543 3.21157 2.3718-.86799-.9918-1.77939-2.0268-2.60398-3.105-.43399-.5175-.86799-1.0781-1.30199-1.6818-.43399-.5607-.86799-1.1644-1.301987-1.7682.694397-1.3368 1.909587-3.1912 3.385177-5.2181 1.47558-2.0269 3.29837-4.1831 5.12115-6.12373C11.1103 5.46005 12.9331 3.73505 14.4521 2.4413c.7378-.60375 1.3888-1.16438 1.953-1.552502.5642-.388125.9982-.69 1.2152-.819375.7811-.388125 1.5623.948747 2.3869 2.846247-.6944 1.2075-1.5624 3.01875-2.3001 4.485.3471-.38812.7811-1.12125 1.2585-1.85437.4774-.73313.9548-1.50938 1.3454-2.02688.1302.345.3472.99188.5208 1.33688-1.4756 2.02687-3.1247 4.18312-4.7739 5.9944.434-.3019.9114-.69 1.3454-1.12128.4773-.43125.9113-.90562 1.3453-1.33687.868-.94875 1.736-1.85438 2.4304-2.54438.6944 1.81125 1.3454 3.66563 1.8662 5.21813.2604.7762.4774 1.4231.651 1.9837.1736.5607.2604.9488.2604 1.1644 0 .1725-.0868.6038-.3472 1.2938-.0868.345-.217.7331-.3906 1.2075-.1302.4743-.3038.9487-.4774 1.5093-.6944 2.2425-1.6058 5.2613-2.3436 8.7113-.7812 3.45-1.4322 7.2881-1.7794 11.04-.3906 3.7519-.4774 7.4175-.434 10.4794-.3471.9487-.7811 1.9837-1.1283 2.9325-.3472.9487-.651 1.8543-.9114 2.5443.3472-.3018 1.3454-1.7249 2.1265-2.8462.0434.6469.0434 1.2937.0868 1.8544.0434.5606.0434 1.0781.0868 1.5093.0434.9057.0434 1.5094 0 1.8113-.0868.5175-.217 1.0781-.3906 1.6387-.1736.5607-.3471 1.0782-.5207 1.5957-.1736.5175-.3906 1.035-.5642 1.4662-.1736.4744-.3472.8625-.4774 1.2075 1.1284 3.9244 2.3435 7.2019 1.9095 6.7275-.4339-.6037-.9981-1.4231-1.5623-2.4581-.2604-.5175-.5642-1.0781-.9114-1.725-.3038-.6038-.6076-1.2938-.9114-1.9838-.3038-.1725-.6076-.3881-.9114-.5606-.3038-.2156-.6076-.4312-.868-.6037-.5642-.3882-1.0416-.7763-1.2586-1.0782-.0434-.0431-.1736-.2156-.3472-.5175-.1736-.3018-.3906-.69-.6944-1.1643-.5642-.9919-1.30198-2.415-2.16997-4.0969-.3906-.8625-.868-1.7681-1.30199-2.76-.217-.5175-.434-.9919-.651-1.5094-.21699-.5175-.43399-1.0781-.65099-1.5956-.434-1.0781-.82459-2.2425-1.25859-3.4069-.3906-1.1644-.82459-2.3719-1.17179-3.6225-1.47559-4.8731-2.47378-9.9619-3.037976-13.9725-.130199-.9919-.260398-1.9406-.347197-2.76-.086799-.8625-.130199-1.5956-.216998-2.2425C.0433996 21.5888 0 20.7263 0 20.4676v-.0863zM21.4828 9.81567c-.3472-.81937-.651-1.5525-.9982-2.02687 0 0-.2604.25875-.6944.64687-.434.38813-1.0416.94875-1.6926 1.5525-1.3453 1.20753-2.9077 2.63063-4.0795 3.27753 0 0 .3038-.4744.7812-1.2938.2604-.4312.5642-.9056.9548-1.5094.3472-.56058.7812-1.20745 1.2586-1.94058-.1736.04313-.5642.345-.9548.64688-.3906.30187-.7378.56062-.7378.56062.1736-.21562.4774-.77625.8246-1.42312.3472-.64688.7378-1.46625 1.1284-2.19938.7811-1.50937 1.5189-2.84625 1.5189-2.84625s-.217-.43125-.5642-.77625c-.3471-.38812-.7811-.69-1.2151-.47437-.1302.08625-.5208.345-1.085.77625-.5642.43125-1.3454 1.07812-2.17 1.85437-1.736 1.50938-3.86258 3.6225-5.77217 5.86503.1736.3881.6076 1.1643 1.34539 2.07.73778.9056 1.86618 1.9837 3.42858 3.0187.217-.6469.3906-1.2937.5642-1.9406.217-.6469.434-1.2506.6076-1.8544-.1302.6469-.2604 1.3369-.434 1.9838-.1302.6468-.2604 1.3368-.3906 1.9837.9982-.1294 1.9964-.3881 2.9512-.69.9548-.3019 1.9095-.69 2.7341-1.035 1.6926-.7331 3.038-1.4231 3.6456-1.725-.3038-.8194-.6076-1.725-.9548-2.50123zM16.8391 23.745c-1.519.4313-3.2116.8194-4.9476 1.0351-.1736 1.9837-.2604 3.9243-.3038 5.8649-.0434 1.8975 0 3.7519.0434 5.5632 1.3888-.5607 2.7776-1.3369 3.9494-2.07 1.1718-.7332 2.17-1.4663 2.7775-1.8975.5642-3.45 1.302-6.9 2.1266-9.8325-.868.3881-2.1266.9056-3.6455 1.3368zm-1.5624 31.1794c.1302-.5175.3038-1.2937.5642-2.1131.217-.8194.4774-1.725.7378-2.6306.2604-.9057.4774-1.725.651-2.4582.0868-.4312.0434-3.4931.3038-7.6331-.6076.5606-1.519 1.2938-2.4738 2.07-.9548.7763-1.953 1.5094-2.6908 2.0269.5642 4.3556 1.3888 7.6331 1.8228 9.3581.1736.7331.3906 1.5525.6076 2.5013l.2604-.2588.217-.8625zm1.1718 9.9188c.2604.4744.4774.9056.651 1.2937.0868.1294 0-.1294-.217-.6468-.1736-.5176-.5208-1.3369-.868-2.2857-.0868-.2587-.1736-.5175-.2604-.7762-.5642-1.5957-1.1718-3.5794-1.6926-5.52-.217-.8625-.434-1.725-.651-2.5013-.1736-.8194-.3472-1.5956-.4774-2.2856-.1302-.69-.2604-1.3369-.3472-1.8975-.0868-.5175-.1736-.9919-.217-1.2506-.0434-.3019-.1302-.8625-.2604-1.5957-.1302-.7331-.217-1.6387-.3906-2.7168-.2604-2.2857-.5642-5.175-.651-8.1938-.0868-2.0269-.0868-4.0969 0-6.0806.0434-.9919.0868-1.9406.1302-2.8463.0434-.4743.0434-.9056.0868-1.3368.0434-.4313.0868-.8625.1302-1.2507.0434-.3018.0434-.6037.0868-.9056.0434-.3019.0434-.5606.0868-.8194.0434-.5175.1302-1.035.1736-1.4662.1302-.8625.217-1.5525.2604-1.8975-.1302.69-.3038 1.4662-.434 2.3287-.1302.8625-.3038 1.8113-.434 2.8032-.0434.4312-.1302.8625-.1736 1.2937-.0434.4313-.0868.9056-.1302 1.38-.0868.9056-.1736 1.8975-.217 2.8463-.0868 1.9406-.1736 3.9675-.1302 5.9081.0434 3.0619.2604 5.9944.5208 8.4525.2604 2.2856.6076 4.1831.8246 5.2612.3472 1.8544.9114 4.3126 1.6058 6.7275.6076 2.0701 1.3454 4.1832 2.0832 5.9513.0868.1725.1302.345.217.5175.217.4744.434 1.035.6944 1.5094zM5.51175 45.3075c.4774 1.3369.9114 2.6307 1.38879 3.8382.4774 1.2075.95479 2.3718 1.43219 3.45.4774 1.0781.91139 2.0268 1.34539 2.9325.82458.4312 2.08318.8193 3.03798 1.0781-1.085-3.5794-1.953-7.7194-2.4304-12.075-.78118-.3019-1.95297-.7763-3.08136-1.38-1.12839-.5606-2.21338-1.2506-2.95118-1.8544.3906 1.3369.8246 2.6738 1.25859 4.0106zM1.82278 30.2569c1.73599 2.0269 4.42677 4.14 8.02894 5.7788 0-3.8382.30378-7.6763.86798-11.385-.99818-.5607-2.69077-1.725-4.33995-3.0619-.8246-.69-1.56239-1.4231-2.21338-2.1131-.651-.69-1.17179-1.38-1.43219-1.8975-.2604.4743-.5208.9056-.651 1.2937.1302.1294.4774.5606.9114 1.1213.21699.3018.47739.6037.73779.9918.2604.345.56419.7332.86799 1.1644.3038.4313.6076.8625.91139 1.2938.3038.4312.651.9056.9548 1.3369.60759.9056 1.21519 1.8112 1.64918 2.6737-.2604-.0431-3.55877-1.1644-7.07414-4.0969.0434.6038.217 3.3206.78119 6.9z" fill="#E27945"/></svg>'
//         featherColors = Math.floor((Math.random() * 2) + 1);
//         featherRotation = Math.floor((Math.random() * 360) + 1);
//         featherpositionY = Math.floor((Math.random() * 100) + 1);
//         featherpositionX = Math.floor((Math.random() * 100) + 1);
//     }
//     document.querySelector(".feathers").innerHTML = html;
// }

// CREATE BACKGROUND PARTICLES
function particles(){
    // window.particlesJS("snowFooterLight",{"particles":{"number":{"value":460,"density":{"enable":true,"value_area":800}},"color":{"value":"#ffffff"},"shape":{"type":"circle","stroke":{"width":0,"color":"#000000"},"polygon":{"nb_sides":5},"image":{"src":"img/github.svg","width":100,"height":100}},"opacity":{"value":1,"random":true,"anim":{"enable":true,"speed":1,"opacity_min":1,"sync":false}},"size":{"value":2,"random":true,"anim":{"enable":false,"speed":4,"size_min":0.3,"sync":false}},"line_linked":{"enable":false,"distance":150,"color":"#ffffff","opacity":0.4,"width":1},"move":{"enable":true,"speed":.3,"direction":"bottom-left","random":true,"straight":false,"out_mode":"out","bounce":false,"attract":{"enable":false,"rotateX":600,"rotateY":600}}},"interactivity":{"detect_on":"canvas","events":{"onhover":{"enable":false,"mode":"bubble"},"onclick":{"enable":false,"mode":"repulse"},"resize":true},"modes":{"grab":{"distance":400,"line_linked":{"opacity":1}},"bubble":{"distance":50,"size":0,"duration":2,"opacity":0,"speed":3},"repulse":{"distance":400,"duration":0.4},"push":{"particles_nb":4},"remove":{"particles_nb":2}}},"retina_detect":true});
    // window.particlesJS("snowFooterHeavy",{"particles":{"number":{"value":160,"density":{"enable":true,"value_area":800}},"color":{"value":"#ffffff"},"shape":{"type":"circle","stroke":{"width":0,"color":"#000000"},"polygon":{"nb_sides":5},"image":{"src":"img/github.svg","width":100,"height":100}},"opacity":{"value":1,"random":true,"anim":{"enable":true,"speed":1,"opacity_min":1,"sync":false}},"size":{"value":3,"random":true,"anim":{"enable":false,"speed":4,"size_min":0.3,"sync":false}},"line_linked":{"enable":false,"distance":150,"color":"#ffffff","opacity":0.4,"width":1},"move":{"enable":true,"speed":1,"direction":"bottom-left","random":true,"straight":false,"out_mode":"out","bounce":false,"attract":{"enable":false,"rotateX":600,"rotateY":600}}},"interactivity":{"detect_on":"canvas","events":{"onhover":{"enable":false,"mode":"bubble"},"onclick":{"enable":false,"mode":"repulse"},"resize":true},"modes":{"grab":{"distance":400,"line_linked":{"opacity":1}},"bubble":{"distance":50,"size":0,"duration":2,"opacity":0,"speed":3},"repulse":{"distance":400,"duration":0.4},"push":{"particles_nb":4},"remove":{"particles_nb":2}}},"retina_detect":true});
    window.particlesJS("snow",{"particles":{"number":{"value":260,"density":{"enable":true,"value_area":800}},"color":{"value":"#ffffff"},"shape":{"type":"circle","stroke":{"width":0,"color":"#000000"},"polygon":{"nb_sides":5},"image":{"src":"img/github.svg","width":100,"height":100}},"opacity":{"value":1,"random":true,"anim":{"enable":true,"speed":1,"opacity_min":1,"sync":false}},"size":{"value":2,"random":true,"anim":{"enable":false,"speed":4,"size_min":0.3,"sync":false}},"line_linked":{"enable":false,"distance":150,"color":"#ffffff","opacity":0.4,"width":1},"move":{"enable":true,"speed":.5,"direction":"bottom-left","random":true,"straight":false,"out_mode":"out","bounce":false,"attract":{"enable":false,"rotateX":600,"rotateY":600}}},"interactivity":{"detect_on":"canvas","events":{"onhover":{"enable":false,"mode":"bubble"},"onclick":{"enable":false,"mode":"repulse"},"resize":true},"modes":{"grab":{"distance":400,"line_linked":{"opacity":1}},"bubble":{"distance":50,"size":0,"duration":2,"opacity":0,"speed":3},"repulse":{"distance":400,"duration":0.4},"push":{"particles_nb":4},"remove":{"particles_nb":2}}},"retina_detect":true});
    window.particlesJS("stars",{"particles":{"number":{"value":760,"density":{"enable":true,"value_area":800}},"color":{"value":"#ffffff"},"shape":{"type":"circle","stroke":{"width":0,"color":"#000000"},"polygon":{"nb_sides":5},"image":{"src":"img/github.svg","width":100,"height":100}},"opacity":{"value":1,"random":true,"anim":{"enable":true,"speed":.2,"opacity_min":.2,"sync":false}},"size":{"value":.5,"random":true,"anim":{"enable":false,"speed":4,"size_min":0.3,"sync":false}},"line_linked":{"enable":false,"distance":150,"color":"#ffffff","opacity":0.4,"width":1},"move":{"enable":false,"speed":.3,"direction":"bottom-left","random":true,"straight":false,"out_mode":"out","bounce":false,"attract":{"enable":false,"rotateX":600,"rotateY":600}}},"interactivity":{"detect_on":"canvas","events":{"onhover":{"enable":false,"mode":"bubble"},"onclick":{"enable":false,"mode":"repulse"},"resize":true},"modes":{"grab":{"distance":400,"line_linked":{"opacity":1}},"bubble":{"distance":50,"size":0,"duration":2,"opacity":0,"speed":3},"repulse":{"distance":400,"duration":0.4},"push":{"particles_nb":4},"remove":{"particles_nb":2}}},"retina_detect":true});
}
export{
  particles,
  MorphingBG,
  Loading,
  createHeroInteractive,
  axiosInstance
}