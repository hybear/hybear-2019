import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    projects: null
  },
  mutations: {
    setProjects(state, projects){
      state.projects = projects;
    },
  },
  actions: {
    getProjects(vuexContext){
      return axios.create().get("https://hybear-12902.firebaseio.com/projects.json")
      .then(result => {
          vuexContext.commit("setProjects", result.data);
          }, error => {
          console.error(error);
      });
    },
  }
});
