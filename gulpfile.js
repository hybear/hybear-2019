var gulp = require('gulp');
var minify = require('gulp-minify');
var run = require('gulp-run');
// var less = require('gulp-less');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var imagemin = require('gulp-imagemin');
var babel = require("gulp-babel");

// gulp.task('run-serve', function() {
//   return run('npm run serve').exec();
// })

gulp.task('minimg',function(){
  gulp.src('./src/assets/img/*')
      .pipe(imagemin())
      .pipe(gulp.dest('./src/assets/img/min/'));
});
gulp.task("babel", function () {
  return gulp.src('./src/assets/js/z-main.js')
    .pipe(babel({ compact: true}))
    .pipe(gulp.dest('./src/assets/js/'));
});

gulp.task('js', function(){
  return gulp.src('./src/assets/js/*.js')
    // .pipe(sourcemaps.init())
    .pipe(concat('main-raw.js'))
    .pipe(minify())
    // .pipe(sourcemaps.write())
    .pipe(gulp.dest('./src/assets/js/'));
});
gulp.task('watch', function() {
  // gulp.watch('./css/less/*.less', ['css']);
  // gulp.watch('./src/assets/sass/*.scss', ['css', 'postcss']);
  gulp.watch('./src/assets/js/z-main.js', ['babel']);
  gulp.watch('./src/assets/js/*.js', ['js']);
  // gulp.watch('./*', ['addcommit']);
});
gulp.task('default', [ 'watch', 'minimg', 'run-serve']);