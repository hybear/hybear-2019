"use strict";function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}function _defineProperties(target,props){for(var i=0;i<props.length;i++){var descriptor=props[i];descriptor.enumerable=descriptor.enumerable||false;descriptor.configurable=true;if("value"in descriptor)descriptor.writable=true;Object.defineProperty(target,descriptor.key,descriptor);}}function _createClass(Constructor,protoProps,staticProps){if(protoProps)_defineProperties(Constructor.prototype,protoProps);if(staticProps)_defineProperties(Constructor,staticProps);return Constructor;}function particles(){particlesJS("box",{"particles":{"number":{"value":160,"density":{"enable":true,"value_area":800}},"color":{"value":"#ffffff"},"shape":{"type":"circle","stroke":{"width":0,"color":"#000000"},"polygon":{"nb_sides":5},"image":{"src":"img/github.svg","width":100,"height":100}},"opacity":{"value":1,"random":true,"anim":{"enable":true,"speed":1,"opacity_min":0,"sync":false}},"size":{"value":3,"random":true,"anim":{"enable":false,"speed":4,"size_min":0.3,"sync":false}},"line_linked":{"enable":false,"distance":150,"color":"#ffffff","opacity":0.4,"width":1},"move":{"enable":true,"speed":1,"direction":"top-right","random":true,"straight":false,"out_mode":"out","bounce":false,"attract":{"enable":false,"rotateX":600,"rotateY":600}}},"interactivity":{"detect_on":"canvas","events":{"onhover":{"enable":true,"mode":"bubble"},"onclick":{"enable":false,"mode":"repulse"},"resize":true},"modes":{"grab":{"distance":400,"line_linked":{"opacity":1}},"bubble":{"distance":50,"size":0,"duration":2,"opacity":0,"speed":3},"repulse":{"distance":400,"duration":0.4},"push":{"particles_nb":4},"remove":{"particles_nb":2}}},"retina_detect":true});}// BACKGROUND CANVAS RENDER
// var createHeroInteractive = function($container) {
// 	'use strict';
// 	// prettier-ignore
// 	var renderer, scene, camera, pixelRatio = Math.min(window.devicePixelRatio, 2),
// 	backgroundScene, backgroundCamera,
// 	geometryBackgroundScene, materialBackgroundScene, meshBackgroundScene,
// 	fov = 45;
// 	var vertexShaderBackgroundScene = `varying vec2 vUv;
// 	void main() {
// 			vUv = uv;
// 			vec3 pos = position;
// 			gl_Position = projectionMatrix * modelViewMatrix * vec4(pos, 1.0);
// 	}`;
// 	var fragmentShaderBackgroundScene = `
// 			vec2 cover(vec2 screenDimensions, vec2 textureDimensions) {
// 					vec2 s = screenDimensions;
// 					vec2 i = textureDimensions;
// 					vec2 st = gl_FragCoord.xy / s;
// 					float rs = s.x / s.y;
// 					float ri = i.x / i.y;
// 					vec2 new = rs < ri ? vec2(i.x * s.y / i.y, s.y) : vec2(s.x, i.y * s.x / i.x);
// 					vec2 offset = (rs < ri ? vec2((new.x - s.x) / 2.0, 0.0) : vec2(0.0, (new.y - s.y) / 2.0)) / new;
// 					vec2 uv = st * s / new + offset;
// 					return uv;
// 			}
// 			vec2 uvCover(in vec2 _uv, vec2 screenDimensions, vec2 textureDimensions) {
// 					vec2 s = screenDimensions;
// 					vec2 i = textureDimensions;
// 					float rs = s.x / s.y;
// 					float ri = i.x / i.y;
// 					vec2 new = rs < ri ? vec2(i.x * s.y / i.y, s.y) : vec2(s.x, i.y * s.x / i.x);
// 					vec2 offset = (rs < ri ? vec2((new.x - s.x) / 2.0, 0.0) : vec2(0.0, (new.y - s.y) / 2.0)) / new;
// 					vec2 uv = _uv * s / new + offset;
// 					return uv;
// 			}
// 			vec3 mod289(vec3 x) { return x - floor(x * (1.0 / 289.0)) * 289.0; }
// 			vec2 mod289(vec2 x) { return x - floor(x * (1.0 / 289.0)) * 289.0; }
// 			vec3 permute(vec3 x) { return mod289(((x*34.0)+1.0)*x); }
// 			float snoise(vec2 v) {
// 					const vec4 C = vec4(0.211324865405187,  // (3.0-sqrt(3.0))/6.0
// 															0.366025403784439,  // 0.5*(sqrt(3.0)-1.0)
// 															-0.577350269189626,  // -1.0 + 2.0 * C.x
// 															0.024390243902439); // 1.0 / 41.0
// 					vec2 i  = floor(v + dot(v, C.yy) );
// 					vec2 x0 = v -   i + dot(i, C.xx);
// 					vec2 i1;
// 					i1 = (x0.x > x0.y) ? vec2(1.0, 0.0) : vec2(0.0, 1.0);
// 					vec4 x12 = x0.xyxy + C.xxzz;
// 					x12.xy -= i1;
// 					i = mod289(i); // Avoid truncation effects in permutation
// 					vec3 p = permute( permute( i.y + vec3(0.0, i1.y, 1.0 ))
// 							+ i.x + vec3(0.0, i1.x, 1.0 ));
// 					vec3 m = max(0.5 - vec3(dot(x0,x0), dot(x12.xy,x12.xy), dot(x12.zw,x12.zw)), 0.0);
// 					m = m*m ;
// 					m = m*m ;
// 					vec3 x = 2.0 * fract(p * C.www) - 1.0;
// 					vec3 h = abs(x) - 0.5;
// 					vec3 ox = floor(x + 0.5);
// 					vec3 a0 = x - ox;
// 					m *= 1.79284291400159 - 0.85373472095314 * ( a0*a0 + h*h );
// 					vec3 g;
// 					g.x  = a0.x  * x0.x  + h.x  * x0.y;
// 					g.yz = a0.yz * x12.xz + h.yz * x12.yw;
// 					return 130.0 * dot(m, g);
// 			}
// 			uniform sampler2D u_textureBg;
// 			uniform sampler2D u_texture;
// 			uniform vec2 u_dimensions;
// 			uniform vec2 u_textureDimensions;
// 			uniform float u_time;
// 			uniform vec2 u_mousePosition;
// 			void main() {
// 					vec2 st = gl_FragCoord.xy / u_dimensions;
// 					vec2 texSt = st;
// 					st.x *= u_dimensions.x / u_dimensions.y;
// 					vec3 bg = texture2D(u_textureBg, uvCover(texSt, u_dimensions, u_textureDimensions)).rgb;
// 					vec3 tex = texture2D(u_texture, uvCover(texSt, u_dimensions, u_textureDimensions)).rgb;
// 					vec3 color = tex;
// 					float noise = snoise(texSt * 0.7 + u_time * 0.05) + snoise(texSt * 0.4 - u_time * 0.03 + 0.3) + snoise(texSt * 0.6 - u_time * 0.01 + 0.5);
// 					noise = smoothstep(0.399, 0.4, noise);
// 					color = mix(
// 							color,
// 							bg,
// 							noise
// 					);
// 					gl_FragColor = vec4(color, 1.);
// 			}`;
// 	var mousePosition = {
// 			x: 0.5,
// 			y: 0.5,
// 	};
// 	var v2MousePosition = new THREE.Vector2(mousePosition.x, mousePosition.y);
// 	var start = Date.now();
// 	var fixedTime = 0,
// 			timeOffset = 0,
// 			dynamicTime = 0;
// 	var w, h, isMobile;
// 	var camX = 0;
// 	var camY = 0;
// 	var isFocused = true,
// 			isInited = false,
// 			inViewport = false;
// 	window.addEventListener('blur', function() {
// 			isFocused = false;
// 	});
// 	window.addEventListener('focus', function() {
// 			isFocused = true;
// 			if (isInited) {
// 					render();
// 			}
// 	});
// 	function updateSize() {
// 			w = $container.offsetWidth;
// 			h = $container.offsetHeight;
// 			isMobile = w < 800;
// 			pixelRatio = isMobile ? Math.min(window.devicePixelRatio, 2) : 1;
// 	}
// 	var loadedAssetsCount = 0;
// 	var requiredAssetsCount = 2;
// 	function hasEverythingLoaded() {
// 			if (loadedAssetsCount === requiredAssetsCount) {
// 					init();
// 			}
// 	}
// 	// prettier-ignore
// 	function M(b,c,f,g,h){var e=-Math.abs(h);this.value=b;this.s=!0;var a=0;this.update=function(d){a+=(d-this.value)*f;a*=g;this.value+=a;this.value<b?(this.value=b,0>a&&(a*=e)):this.value>c&&(this.value=c,0<a&&(a*=e));this.s=Math.abs(a)<.001*(c-b)&&.1>Math.abs(this.value-d)?!0:!1;return this.value};this.setValue=function(d){a=0;this.s=!0;return this.value=Math.min(c,Math.max(b,d))}}
// 	var textureLoader = new THREE.TextureLoader();
// 	var imageTexture1Width, imageTexture1Height;
// 	var imageTexture1 = textureLoader.load('https://www.girafa.com.br/hs/aniversariogirafa2018/img/BG1.png', function(texture) {
// 			imageTexture1Width = texture.image.width;
// 			imageTexture1Height = texture.image.height;
// 			texture.generateMipmaps = false;
// 			texture.wrapS = texture.wrapT = THREE.ClampToEdgeWrapping;
// 			texture.minFilter = THREE.LinearFilter;
// 			loadedAssetsCount++;
// 			hasEverythingLoaded();
// 	});
// 	var imageTexture2Width, imageTexture2Height;
// 	var imageTexture2 = textureLoader.load('https://www.girafa.com.br/hs/aniversariogirafa2018/img/BG-NIVER-2.png', function(texture) {
// 			imageTexture2Width = texture.image.width;
// 			imageTexture2Height = texture.image.height;
// 			texture.generateMipmaps = false;
// 			texture.wrapS = texture.wrapT = THREE.ClampToEdgeWrapping;
// 			texture.minFilter = THREE.LinearFilter;
// 			loadedAssetsCount++;
// 			hasEverythingLoaded();
// 	});
// 	function init() {
// 			updateSize();
// 			scene = new THREE.Scene();
// 			camera = new THREE.PerspectiveCamera(fov, w / h, 1, 500);
// 			camera.position.z = isMobile ? 200 : 150;
// 			scene.add(camera);
// 			materialBackgroundScene = new THREE.ShaderMaterial({
// 					uniforms: {
// 							u_textureBg: {type: 't', value: imageTexture1},
// 							u_texture: {type: 't', value: imageTexture2},
// 							u_textureDimensions: {
// 									type: 'v2',
// 									value: new THREE.Vector2(
// 											imageTexture2Width,
// 											imageTexture2Height
// 									),
// 							},
// 							u_dimensions: {
// 									type: 'v2',
// 									value: new THREE.Vector2(w * pixelRatio, h * pixelRatio),
// 							},
// 							u_time: {type: 'f', value: 0},
// 							u_mousePosition: {type: 'v2', value: v2MousePosition},
// 					},
// 					fragmentShader: fragmentShaderBackgroundScene,
// 					vertexShader: vertexShaderBackgroundScene,
// 			});
// 			var backgroundMesh = new THREE.Mesh(
// 					new THREE.PlaneGeometry(2, 2, 0),
// 					materialBackgroundScene
// 			);
// 			backgroundScene = new THREE.Scene();
// 			backgroundCamera = new THREE.Camera();
// 			backgroundScene.add(backgroundCamera);
// 			backgroundScene.add(backgroundMesh);
// 			renderer = new THREE.WebGLRenderer({antialias: true, alpha: false});
// 			renderer.setPixelRatio(pixelRatio);
// 			renderer.setSize(w, h);
// 			renderer.setClearColor(new THREE.Color('rgb(255,255,255)'));
// 			renderer.autoClear = false;
// 			$container.appendChild(renderer.domElement);
// 			window.addEventListener('resize', handleResize);
// 			if ('ontouchstart' in window) {
// 					document.body.addEventListener('touchmove', handleTouchMove);
// 			} else {
// 					document.body.addEventListener('mousemove', handleMouseMove);
// 			}
// 			isInited = true;
// 			render();
// 	}
// 	function handleResize() {
// 			updateSize();
// 			renderer.setSize(w, h);
// 			renderer.setPixelRatio(pixelRatio);
// 			camera.aspect = w / h;
// 			camera.position.z = isMobile ? 200 : 150;
// 			var v2WH = new THREE.Vector2(w, h);
// 			materialBackgroundScene.uniforms.u_dimensions.value = v2WH;
// 			camera.updateProjectionMatrix();
// 	}
// 	function handleTouchMove(e) {
// 			var touch = e.touches[0];
// 			mousePosition.x = touch.pageX / w;
// 			mousePosition.y = touch.pageY / h;
// 	}
// 	function handleMouseMove(e) {
// 			mousePosition.x = e.pageX / w;
// 			mousePosition.y = e.pageY / h;
// 	}
// 	function render(timestamp) {
// 			if (isFocused) {
// 					camX =
// 							((mousePosition.x - 0.5) * (isMobile ? 100 : 60) -
// 									camera.position.x) *
// 							0.05;
// 					camY =
// 							((mousePosition.y - 0.5) * (isMobile ? 60 : 40) -
// 									camera.position.y) *
// 							0.05;
// 					fixedTime = 0.0001 * (Date.now() - start);
// 					timeOffset += (Math.abs(camX) + Math.abs(camY)) / 20;
// 					dynamicTime = fixedTime + timeOffset;
// 					materialBackgroundScene.uniforms.u_time.value = dynamicTime;
// 					camera.position.x += camX;
// 					camera.position.y += camY;
// 					camera.position.z += camY;
// 					camera.lookAt(scene.position);
// 					renderer.clear();
// 					renderer.render(backgroundScene, backgroundCamera);
// 					requestAnimationFrame(render);
// 			}
// 	}
// };
// createHeroInteractive(document.getElementById('container'));
$(function(){particles();});{var MorphingBG=/*#__PURE__*/function(){function MorphingBG(el){_classCallCheck(this,MorphingBG);this.DOM={};this.DOM.el=el;this.DOM.paths=Array.from(this.DOM.el.querySelectorAll('path'));this.animate();}_createClass(MorphingBG,[{key:"animate",value:function animate(){this.DOM.paths.forEach(function(path){setTimeout(function(){anime({targets:path,duration:anime.random(5000,8000),easing:[0.5,0,0.5,1],d:path.getAttribute('pathdata:id'),loop:true,direction:'alternate'});},anime.random(0,1000));});}}]);return MorphingBG;}();;new MorphingBG(document.querySelector('svg.main-wave'));};