const path = require('path');
const HtmlWebpackExternalsPlugin = require('html-webpack-externals-plugin');
const HtmlWebpackIncludeAssetsPlugin = require('html-webpack-include-assets-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const glob = require("glob");

module.exports = {
    pages: {
      index: {
        // entry for the page
        entry: './src/main.js',
        // the source template
        template: './public/index.html',
        // output as dist/index.html
        filename: './index.html',
        // when using title option,
        // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
        title: 'Hybear - Patrick Gomes UX Designer and Web Developer',
        // chunks to include on this page, by default includes
        // extracted common chunks and vendor chunks.
        chunks: ['chunk-vendors', 'chunk-common', 'index']
      }
      // when using the entry-only string format,
      // template is inferred to be `public/subpage.html`
      // and falls back to `public/index.html` if not found.
      // Output filename is inferred to be `subpage.html`.
    //   subpage: 'src/subpage/main.js'
    },
    publicPath: process.env.NODE_ENV === 'production'
    ? ''
    : '/',
    pluginOptions: {
        'style-resources-loader': {
            'patterns': [
                path.resolve(__dirname, 'src/assets/sass/*.scss'),
            ]
        }
    },
    configureWebpack:{
      plugins:[
        new HtmlWebpackPlugin(),
        new HtmlWebpackIncludeAssetsPlugin({
          assets: glob.sync('./src/assets/js/*.+(js)', { cwd: './public' }),
          append: false
        })
      ]
    },
    pwa: {
      name: 'Hybear',
      themeColor: '#E27945',
      msTileColor: '#091124',
      appleMobileWebAppCapable: 'yes',
      appleMobileWebAppStatusBarStyle: 'black',
  
      // configure the workbox plugin
      workboxPluginMode: 'InjectManifest',
      workboxOptions: {
        // swSrc is required in InjectManifest mode.
        swSrc: 'src/service-worker.js',
        // ...other Workbox options...
      }
    },
    chainWebpack: config => {
      config.externals({
      });
    },
  };